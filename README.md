# Social Value Orientation

Contents:
* <a href="#introduction">Introduction</a>
* <a href="#experiment">Experiment</a>
* <a href="#population">Population</a>
* <a href="#simulation-games">Simulation games</a>
* <a href="#real-world-behavior">Real world behavior</a>
* <a href="#fairness">Fairnesss</a>
* <a href="procedural-justice">Procedural justice</a>
* <a href="#voice">Voice</a>
* <a href="#negotiation">Negotiation</a>
* <a href="#getting-to-yes">Getting to yes</a>
* <a href="#nudges">Utility</a></h2>
* <a href="#nudges">Nudges</a></h2>
* <a href="#thanks">Thanks</a>


<h2><a name="introduction">Introduction</a></h2>

Social value orientation (SVO) is a psychological trait that is defined as an individual’s natural preference with respect to the allocation of resources.

SVO has 2 categories:

* A "pro-self" person wants to maximize their own outcome.

* A "pro-social" person wants to maximize their group's outcomes.

SVO has 3 subcategories:

* A pro-self "competitor" wants to maximize their own outcome relative to others’ outcomes.

* A pro-self "individualist" wants to maximize their own outcome regardless of others’ outcomes.

* A pro-social "cooperator" wants to maximize their group's outcomes, and wants all outcomes to be equivalent.


<h2><a name="experiment">Experiment</a></h2>

Choose which option sounds best to you:

* Option A: 480 points for yourself and 80 points for the other

* Option B: 540 points for you and 280 points for the other

* Option C: 480 points for you and 480 points for the other

Choose which option sounds like it describes you:

* Option A: I like to win relative to other people.

* Option B: I like to gain the largest share of resources for myself. 

* Option C: I like to equalize outcomes.

Scoring:

* If you chose Option A, then you're a proself competitor.

* If you chose Option B, then you're a proself individualist.
 
* If you chose Option C, then you're prosocial cooperator.

If you chose different options in each experiment, then you're a mix.


<h2><a name="population">Population</a></h2>

SVO population results for people who are WEIRD (western, educated, industrialized, rich, democratic):

* Competitors: ~ 1/8 of people.

* Individualists: ~ 1/4 of people.

* Cooperators: ~ 1/2 of people.

SVO testing is extensively developed and has been tweaked over the years. 

SVO testing complements the "Big 5" personality variables in psychology: extraversion, agreeableness, openness, conscientiousness, neuroticism.


<h2><a name="simulation-games">Simulation games</a></h2>

People who measure as "cooperators" through an SVO measurement tool display greater actual cooperation in experimental games involving the distribution of resources, such as the prisoner’s dilemma, the ultimatum game, and the dictator game, than people who have been classified as individualists and competitors.

SVO has significant effects on the outcome of such games, directly affecting how resources are divided among participants.

SVO is also correlated with process behaviors, such as the strategic use of fairness in negotiation. 

* Proself individualists are tougher and less problem-solving in negotiation settings. 

Researchers have distinguished between true concern for fairness, demonstrated by prosocials, and strategic use of fairness by proselfs.


<h2><a name="real-world-behavior">Real world behavior</a></h2>

SVO correlates with real world behavior. 

Prosocial and proself comparisons:

* Prosocials donate more to others, especially to the poor and the sick.

* Prosocials are more likely to take public transportation, and also to link that decision to issues of collective welfare.

* Prosocials care most about the equality of procedures, while proself people mainly assess their own procedures without regard to others’ experiences.

* Proselfs have a heightened sensitivity to whether or not they have had a voice in a particular process; this leads to what psychologists call the “egocentric justice hypothesis.”


<h2><a name="fairness">Fairnesss</a></h2>

Social value orientation sheds light on how people perceive fairness:

* Proselfs are largely indifferent to procedures by others.

* Prosocials are more attuned to the procedures that the other party experiences.


<h2><a name="procedural-justice">Procedural justice</a></h2>

Social value orientation sheds light on how people perceive procedural justice.

Procedural justice is important to both proself and prosocial individuals. 

Enhancing procedural justice across the board is a perfect response to the differences in perspective between prosocials and proselfs.

Procedural justice is a stronger predictor of certain positive behaviors among proself than among prosocial individuals.


<h2><a name="voice">Voice</a></h2>

In one study, researchers varied the voice opportunities for both sides in a dispute. People were either granted or denied voice in a decision-making process.

Both groups (proselfs and prosocials) were influenced by their own voice process when the other party had a voice.

* Proselfs found the process procedurally just, regardless of whether the other party was granted voice or was denied voice. Prosocials did not.

* Prosocials even preferred a no voice condition (in which both parties were denied voice) to a condition where only one side received a voice.


<h2><a name="negotiation">Negotiation</a></h2>

The two dominant frames in negotiation theory:

* Competitive 

  * A.k.a. positional negotiation model.

  * Use techniques such as small concessions, withholding information, beginning with inflated numbers, and other tactics designed to ensure the smallest outcome for one’s adversary along with the highest outcome for oneself.

* Collaborative 

  * A.k.a. principled negotiation model.

  * Uses techniques such as asking negotiators to stand in the other party’s shoes, to understand all parties’ interests, and to brainstorm options for mutual gain. 

SVO offers a useful way to understand the clash between these approaches to negotiation. 

Both models purport to provide a way for individuals to maximize their own outcome, but in different ways.

* Competitive (positional) may be better for people with proself SVO.

* Collaborative (principled) may be better for people with prosocial SVO. 

Negotiation lawyers report that most negotiations are more collaborative than competitive, and that collaborative, problem-solving negotiators were perceived as more effective than their positional counterparts.

Additionally, research suggests that a prosocial SVO also fosters more collaborative behavior during negotiation. Prosocial individuals “create” more procedural justice during negotiations, while proself individuals “create” less.


<h2><a name="getting-to-yes">Getting to yes</a></h2>

An understanding of SVO may provide negotiators with a useful insight: even a negotiator with a proself SVO may become convinced of the strategic need to care about the other party’s outcome. 

Acting collaborative can be the best tool to maximize your own interests, and convincing the other party – even if it is purely self-interested – that it needs to care about your interests in order to meet its own goals can be a tremendously effective negotiation strategy.

This may be the most provocative idea in the book "Getting to Yes". 


<h2><a name="utility">Utility</a></h2>

SVO challenges the utility assumptions of law and economics. We are not all – or even mostly – rational actors in the sense of wishing our own utility to be maximized across all settings. Real human psychology does not match the rational-actor assumptions; we must consider actual human behavior.

* The basic economics framework to maximize collective utility has been understood to be to maximize individual utility and then aggregate. 

* Each individual, acting in the service of his or her own individual utility, will collectively maximize societal utility.

But a focus on aggregation of individual utility blinds us to the importance of relational conceptions of utility:

* A significant portion (1/8) of the general population is proself competitors, and they value utility in relation to besting others, even when their own economic results may suffer. 

* A significant portion (1/2) of the general population is prosocial collaborators, and they value utility in relation to an even split of resources among others, even at a loss to self, and potentially even when the total pie is smaller. 

Our understanding of utility needs to be more capacious. Thinking about individual decision making from the perspective of individuals who are focused on relational outcomes offers a radically different angle from which to view utility.


<h2><a name="nudges">Nudges</a></h2>

To stimulate proself behavior or prosocial behavior, we can leverage the “nudge” movement, which seeks to use small interventions to change individual behavior and overcome cognitive biases.

* SVO helps to surface the idea that efficiency and individual utility are choices – choices that not everyone is naturally inclined to make.

* When a population is split between being naturally inclined toward proself and prosocial, then people may hotly contest any normative direction for nudging.


<h2><a name="thanks">Thanks</a></h2>

* This information comes from [Social Value Orientation and the Law by Rebecca Hollander-Blumoff](https://ssrn.com/abstract=2944222)

* This page is maintained by Joel Parker Henderson (joel@joelparkerhenderson.com)
